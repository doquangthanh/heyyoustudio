const express = require("express");
const methodOverride = require("method-override");
const cookieParser = require("cookie-parser");
const dotenv = require("dotenv");
const session = require("express-session");
const { ApolloServer } = require('apollo-server-express')
const route = require("./routes");
const sequelize = require("./config/db");
const cors = require("cors");
const port = process.env.PORT || 8000;
dotenv.config();
const app = express();
app.use(cors((origin = ["http://localhost:4200"])));
app.use(
  session({
    secret: "secret",
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false,
      maxAge: 1000 * 60 * 60 * 24,
    },
  })
);
// const server = new ApolloServer({
//   typeDefs,
// })
// server.applyMiddleware(app)
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// connect database
sequelize
  .authenticate()
  .then((data) => {
    console.log("sql to start");
  })
  .catch((err) => {
    console.log("sql to die");
  });

app.use(methodOverride("_method"));
//router
route(app);
// listen sever...
app.listen(port, () => {
  console.log(` server to connect http://localhost:${port}`);
});
